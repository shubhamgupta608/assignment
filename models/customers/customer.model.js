//Customer Model
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const customerSchema = new Schema({
  location: {
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    },
  },
  user_id: {
    type: Number,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
})
//Create Index for location
customerSchema.index({ location: "2dsphere" });
module.exports = mongoose.model('CustomerModel', customerSchema) 
