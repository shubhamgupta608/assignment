const mongoose = require('../mongo/connection')
const CustomerModel = mongoose.CustomerModel;

//Saving the customer data in mongodb
async function create(userParam) {
    try {
        for (let obj of userParam) {
            // console.log("User is:", obj)
            const cust = {
                "location": {
                    "type": 'Point',
                    "coordinates": [+obj.longitude, +obj.latitude]  //+ convert String to number
                },
                "user_id": obj["user_id"],
                "name": obj["name"]
            }

            let customerDetail = new CustomerModel(cust)

            await customerDetail.save();
        }
        console.log("Schema was created and data saved into the database.")
    }
    catch (err) {
        console.log(err)
    }
}
//Reading the customer data from mongodb
async function getData() {
    try {
        var pipeline = [{
            $geoNear: {
                near: { type: "Point", coordinates: [-6.257664, 53.339428] },//dublin longitude and latitude 
                distanceField: "dist.calculated",
                maxDistance: 100000,                                         //convert 100 kms to meters
                distanceMultiplier: 0.001,
                includeLocs: "dist.location",
                spherical: true
            }
        },
        {
            $project: { user_id: 1, name: 1, _id: 0 }
        },
        {
            $sort: { user_id: 1 }
        },
        ]
        let data = await CustomerModel.aggregate(pipeline)
        console.log("Data is:", data)
        return { data }
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    create,
    getData
};
