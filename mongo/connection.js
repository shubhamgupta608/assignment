//Connection with Mongodb using mongoose
const config = require('../config/config')
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI || config.connectionUrl, { useNewUrlParser: true, useCreateIndex: true });
mongoose.Promise = global.Promise;

const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connectionn established successfully!");
})
module.exports = {
    CustomerModel: require('../models/customers/customer.model')
}
