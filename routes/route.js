const express = require('express');
const router = express.Router();
const customerService = require('../services/customer.service');
var fs = require("fs");

//Check route for Initiate
router.route('/').get((req, res) => {
    res.json({
        msg: "Initiated"
    })
})

//Insert Route for insert data to customer.txt to atlas 
router.route('/insertData').get(createcustomer);

//Get route for customerdata 
router.route('/getData').get(compute);

//create and saving the data in atlas
function createcustomer(req, res, next) {
    fs.readFile("customers.txt", "utf-8", (err, data) => {
        if (err) { console.log(err) }
        let splittedData = data.split("\n");
        var locationObjs = splittedData.map(JSON.parse);

        customerService.create(locationObjs)
            .then(() => res.status(200).json({ data: 'Added Successfully' }))
            .catch(err => next(err));
    })
}

//getting the data from atlas
function compute(req, res, next) {
    customerService.getData()
        .then((data) => res.json(data))
        .catch(err => next(err));
}
module.exports = router;